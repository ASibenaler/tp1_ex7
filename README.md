## Exercice 7

# Objectif
L'objectif de cet exercice est de compléter la classe de test précédement créée par un système de logs.

# Réalisation
Pour créer des logs, on utilise la bibliothèque *logging*.
Dans les méthodes d'initialisation des classes de test, on configure le système de logs pour que les messages soient affichés dans un fichier texte. On ajoute des messages de log dans chaque méthode de test.

# Résultat
En exécutant le code, le fichier texte suivant est généré :

	INFO:root:Test division int OK
	INFO:root:Test division int-char ERREUR
	INFO:root:Test division par 0 ERREUR
	INFO:root:Test produit int OK
	INFO:root:Test produit int-char ERREUR
	INFO:root:Test soustraction int OK
	INFO:root:Test soustraction int-char ERREUR
	INFO:root:Test somme int OK
	INFO:root:Test somme int-char ERREUR

